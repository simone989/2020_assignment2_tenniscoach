\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {section}{\numberline {1}Application Overview}{3}{section.1}%
\contentsline {subsection}{\numberline {1.1}Introduzione}{3}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Descrizione dell'applicazione}{3}{subsection.1.2}%
\contentsline {section}{\numberline {2}Workflow}{4}{section.2}%
\contentsline {section}{\numberline {3}Background Study}{5}{section.3}%
\contentsline {subsection}{\numberline {3.1}Soluzioni attualmente presenti sul mercato}{5}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Vantaggio competitivo}{5}{subsection.3.2}%
\contentsline {section}{\numberline {4}Stakeholders List}{6}{section.4}%
\contentsline {section}{\numberline {5}Survey and Interview}{7}{section.5}%
\contentsline {subsection}{\numberline {5.1}Players}{7}{subsection.5.1}%
\contentsline {subsubsection}{\numberline {5.1.1}Common Questions}{7}{subsubsection.5.1.1}%
\contentsline {subsubsection}{\numberline {5.1.2}Main Questions}{8}{subsubsection.5.1.2}%
\contentsline {subsection}{\numberline {5.2}Tennis Trainer}{10}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Tennis Court Owners}{11}{subsection.5.3}%
\contentsline {subsection}{\numberline {5.4}Marketing And Sales}{12}{subsection.5.4}%
\contentsline {subsection}{\numberline {5.5}Developers}{13}{subsection.5.5}%
