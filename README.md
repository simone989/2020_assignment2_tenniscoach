# Processo e Sviluppo Software - Assignment 2

This is the assignment 2 of "Processo e Sviluppo del Software (Blended)"
course Unimib.

> **GROUP COMPOSITION:**

*  Simone Di Mauro
*  Alessandro Maggi

> **PIPELINE STAGE**

Pipeline compile the .tex files and create an artifact.

**REQUIREMENT**

### MACOS

https://mg.readthedocs.io/latexmk.html#installation


### Run local

```
latexmk -pdf main.tex
```
